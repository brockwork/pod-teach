'use strict';

const companies = require('../data/companies.json');

function locatedInScottsdale(company) {
	return 'Scottsdale' === company.City; // if truthy then keep item
}

function nameAndPhoneNumber(company) {
	return { // return what new object will look like
		name: company.BusinessName,
		phone: company.Phone
	};
}

const result = companies.filter(locatedInScottsdale).map(nameAndPhoneNumber);

console.log(result, result.length);
/*
 [ { name: 'Arizona Healthcare Realty Llc', phone: 6029803171 },
  { name: 'Car Wash Brokers Inc', phone: 6027871100 },
  { name: 'Colorado Cu Realty Services, Llc', phone: 4803621790 },
  { name: 'Commercial Consultants Llc', phone: 3037994800 },
  { name: 'Denver Luxury Real Estate Llc', phone: 3039491512 } ]
 */
// 5
