'use strict';

const companies = require('../data/companies.json');

function countLicensedSince1996(memo, company) {
	const issueDate = new Date(company['LicenseFirstIssueDate']);

	if (issueDate.getFullYear() <= 1996) {
		memo += 1;
	}
	return memo
}

const result = companies.reduce(countLicensedSince1996, 0);

console.log(result);
// 316
