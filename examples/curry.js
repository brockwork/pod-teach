'use strict';

var R = require('ramda');
const companies = require('../data/companies.json');

function filterBy(property, value, company) {
	if (value === company[property]) {
		return company;
	}
}

const cityOfScottsdale = R.curry(filterBy)('City', 'Scottsdale');

const result = companies.filter(cityOfScottsdale);

console.log(result, result.length);
// 5